package com.appdev360.syncadapterdemo;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.provider.SyncStateContract;
import android.util.Base64;
import android.util.Log;

import com.appdev360.syncadapterdemo.data.constant.GeneralConstants;
import com.appdev360.syncadapterdemo.data.constant.NetworkConstants;
import com.appdev360.syncadapterdemo.data.remote.NetworkService;
import com.appdev360.syncadapterdemo.injection.component.ApplicationComponent;
import com.appdev360.syncadapterdemo.injection.component.DaggerApplicationComponent;
import com.appdev360.syncadapterdemo.injection.module.ApplicationModule;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abubaker on 1/23/18.
 */

public class SyncApplication extends Application{

    ApplicationComponent mApplicationComponent;

    private static SyncApplication sInstance;
    private NetworkService syncApplicationAPI;
    private OkHttpClient client;


    public static SyncApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }



    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        initializeRetrofit();

        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }



    public static SyncApplication get(Context context) {
        return (SyncApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();


        }
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    public ApplicationComponent getAppComponent() {
        return mApplicationComponent;
    }


        private void initializeRetrofit(){
            ////// Retrofit configurations

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            final Request original = chain.request();
                            Response response;

                                Request request = original.newBuilder()
                                       // .header("Authorization", "Bearer " + SyncStateContract.Constants.TOKEN)
                                        .method(original.method(), original.body())
                                        .build();
                                response = chain.proceed(request);
                           /* } else {
                                response = chain.proceed(original);
                            }*/
                            return response;
                        }
                    })
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
            RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(NetworkConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(rxAdapter)
                    .client(client)
                    .build();
            syncApplicationAPI = retrofit.create(NetworkService.class);
        }


    public NetworkService getNetworkApi() {
        return syncApplicationAPI;
    }
}
