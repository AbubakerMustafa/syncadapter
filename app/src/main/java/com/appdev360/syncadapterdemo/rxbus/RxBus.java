package com.appdev360.syncadapterdemo.rxbus;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by abubaker on 1/25/18.
 */
@Singleton
public class RxBus {

    private PublishSubject<Object> mBus;

    @Inject
    public RxBus() {
        mBus = PublishSubject.create();
    }

    /**
     * publish an object (usually an Event) to the bus
     */
    public void publish(Object o) {
        mBus.onNext(o);

    }

    /**
     * Observable that will emmit everything posted to the event mBus.
     */
    public Observable<Object> toObservable() {
        return mBus;
    }



    /**
     * Observable that only emits events of a specific class.
     * Use this if you only want to subscribe to one type of events.
     */
    public <T> Observable<T> filteredObservable(final Class<T> eventClass) {
        return mBus.ofType(eventClass);
    }

}
