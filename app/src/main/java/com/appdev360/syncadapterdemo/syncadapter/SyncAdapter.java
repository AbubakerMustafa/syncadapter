package com.appdev360.syncadapterdemo.syncadapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.appdev360.syncadapterdemo.SyncApplication;
import com.appdev360.syncadapterdemo.data.DataManager;
import com.appdev360.syncadapterdemo.data.model.BaseModel;
import com.appdev360.syncadapterdemo.data.model.Data;
import com.appdev360.syncadapterdemo.data.model.User;
import com.appdev360.syncadapterdemo.rxbus.RxBus;

import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by abubaker on 1/24/18.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    /*@Inject
    DataManager dataManager;
    @Inject
    RxBus rxBus;*/

    /**
     * Content resolver, for performing database operations.
     */
    private final ContentResolver mContentResolver;

    private static final String TAG = SyncAdapter.class.getSimpleName();


    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
     //   SyncApplication.get(context).getAppComponent().inject(this);

    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        Log.i(TAG, "onPerformSync() was called");
        refreshProduct();
    }

    @Override
    public void onSyncCanceled() {
        super.onSyncCanceled();
        Log.i(TAG, "sync was cancelled");
    }


    private void refreshProduct(){

        SyncApplication.getInstance().getNetworkApi().getUserList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.immediate())
                .subscribe(new Observer<BaseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BaseModel baseModel) {

                        Data data = baseModel.getData();
                        List<User> users = data.getUsers();
                        Log.d(TAG, users.get(0).getBiography());


                    }
                });
    }

/*
    private void getUserList(){

        dataManager.getNetworkService().getUserList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d(TAG, e.getMessage());
                    }

                    @Override
                    public void onNext(BaseModel model) {

                        rxBus.publish(model);

                    }
                });
    }*/


}
