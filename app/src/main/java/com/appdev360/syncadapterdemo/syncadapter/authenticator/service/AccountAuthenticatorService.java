package com.appdev360.syncadapterdemo.syncadapter.authenticator.service;

import android.accounts.Account;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.appdev360.syncadapterdemo.syncadapter.authenticator.Authenticator;

/**
 * Created by abubaker on 1/24/18.
 */

public class AccountAuthenticatorService extends Service{

    private Authenticator authenticator;
    public static final String ACCOUNT_NAME = "Account";



    @Override
    public void onCreate() {
        super.onCreate();
        authenticator = new Authenticator(this);
    }

    public static Account GetAccount(String accountType) {
        // Note: Normally the account name is set to the user's identity (username or email
        // address). However, since we aren't actually using any user accounts, it makes more sense
        // to use a generic string in this case.
        //
        // This string should *not* be localized. If the user switches locale, we would not be
        // able to locate the old account, and may erroneously register multiple accounts.
        final String accountName = ACCOUNT_NAME;
        return new Account(accountName, accountType);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }
}
