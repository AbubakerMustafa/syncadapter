package com.appdev360.syncadapterdemo.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncRequest;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.appdev360.syncadapterdemo.R;
import com.appdev360.syncadapterdemo.injection.ApplicationContext;
import com.appdev360.syncadapterdemo.syncadapter.authenticator.service.AccountAuthenticatorService;

import javax.inject.Inject;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by abubaker on 1/24/18.
 */

public class SyncManager {

    private static final String TAG = SyncManager.class.getSimpleName();

    private static final long SYNC_FREQUENCY = 60 * 6;  //  (in seconds)
    private static final long FLEX_FREQUENCY = 60 * 3;  //  (in seconds)
    private static final String CONTENT_AUTHORITY = "com.appdev360.syncadapterdemo";
    // Value below must match the account type specified in res/xml/syncadapter.xml
    public static final String ACCOUNT_TYPE = "com.appdev360.syncadapterdemo.account";
    private static final String PREF_SETUP_COMPLETE = "setup_complete";
    /**
     * Content provider authority.
     */

    @SuppressWarnings ("MissingPermission")
    public static void beginPeriodicSync(Context context) {
        boolean newAccount = false;
        boolean setupComplete = PreferenceManager
                .getDefaultSharedPreferences(context).getBoolean(PREF_SETUP_COMPLETE, false);

        // Create account, if it's missing. (Either first run, or user has deleted account.)
        Account account = AccountAuthenticatorService.GetAccount(ACCOUNT_TYPE);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        if (!accountManager.addAccountExplicitly(account, null, null)) {

            // newAccount = true;
            account = accountManager.getAccountsByType(ACCOUNT_TYPE)[0];
        }


        // Inform the system that this account supports sync
        ContentResolver.setIsSyncable(account, CONTENT_AUTHORITY, 1);
        // Inform the system that this account is eligible for auto sync when the network is up
        ContentResolver.setSyncAutomatically(account, CONTENT_AUTHORITY, true);
        // Recommend a schedule for automatic synchronization. The system may modify this based
        // on other scheduled syncs and network utilization.
      //  ContentResolver.addPeriodicSync(account, CONTENT_AUTHORITY, new Bundle(),SYNC_FREQUENCY);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(SYNC_FREQUENCY, FLEX_FREQUENCY)
                    .setSyncAdapter(account, CONTENT_AUTHORITY)
                    .setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, CONTENT_AUTHORITY, new Bundle(), SYNC_FREQUENCY);
        }


        // Schedule an initial sync if we detect problems with either our account or our local
        // data has been deleted. (Note that it's possible to clear app data WITHOUT affecting
        // the account list, so wee need to check both.)
        if (newAccount || !setupComplete) {
          //  TriggerRefresh();
            PreferenceManager.getDefaultSharedPreferences(context).edit()
                    .putBoolean(PREF_SETUP_COMPLETE, true).commit();
        }
    }

 /*  public void syncImmediately() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(account, authority, settingsBundle);
    }*/

/*    private void setAccountSyncable() {
        if (ContentResolver.getIsSyncable(account, authority) == 0) {
            ContentResolver.setIsSyncable(account, authority, 1);
        }
    }*/

    public static void TriggerRefresh() {
        Bundle b = new Bundle();
        // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(
                AccountAuthenticatorService.GetAccount(ACCOUNT_TYPE), // Sync account
                CONTENT_AUTHORITY,                 // Content authority
                b);                                             // Extras
    }



}
