package com.appdev360.syncadapterdemo.ui.mvpviews;

import com.appdev360.syncadapterdemo.data.model.Error;
import com.appdev360.syncadapterdemo.data.model.User;
import com.appdev360.syncadapterdemo.ui.base.MvpView;

import java.util.List;

/**
 * Created by abubaker on 1/23/18.
 */

public interface UserView extends MvpView{

    void showUsers(List<User> users);

    void showError(Error error);

    void noUserFound(String string);
}
