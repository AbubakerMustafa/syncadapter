package com.appdev360.syncadapterdemo.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.appdev360.syncadapterdemo.data.DataManager;
import com.appdev360.syncadapterdemo.injection.component.ActivityComponent;
import com.appdev360.syncadapterdemo.util.GeneralUtils;

import javax.inject.Inject;

import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment {
    public static final String ARGS_INSTANCE = "co.appdev.boilerplate.argsInstance";

    public View parent;
    public FragmentNavigation mFragmentNavigation;
    int mInt = 0;

    @Inject
    public DataManager dataManager;
    public String userToken;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityComponent component = ((BaseActivity) getActivity()).activityComponent();
        if (component == null) {
            GeneralUtils.restartApp(getActivity());
            return;
        }
        component.inject(this);
        Bundle args = getArguments();
        if (args != null) {
            mInt = args.getInt(ARGS_INSTANCE);
        }
        userToken = dataManager.getPreferencesHelper().getToken();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigation) {
            mFragmentNavigation = (FragmentNavigation) context;
            mFragmentNavigation.navigationTitle(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parent = inflater.inflate(getLayoutId(), container, false);
        parent.setClickable(true);
        parent.requestFocus();
        ButterKnife.bind(this, parent);
//            parent.setOnClickListener(this);
        initViews(parent);
        updateFragmentReference();
        return parent;
    }

    public interface FragmentNavigation {
        public void pushFragment(Fragment fragment, boolean detach);

        public void replaceFragment(Fragment fragment);

        public void navigationTitle(Fragment fragment);

        public void popCurrentFragment();
    }

    public abstract void initViews(View parentView);

    public abstract int getLayoutId();

    public abstract void updateFragmentReference();

    public abstract void updateActionState(boolean action, View v);

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
