package com.appdev360.syncadapterdemo.ui.syncscreen.fragment;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.appdev360.syncadapterdemo.R;

import com.appdev360.syncadapterdemo.data.model.BaseModel;
import com.appdev360.syncadapterdemo.data.model.Error;
import com.appdev360.syncadapterdemo.data.model.User;
import com.appdev360.syncadapterdemo.rxbus.RxBus;
import com.appdev360.syncadapterdemo.syncadapter.SyncManager;
import com.appdev360.syncadapterdemo.ui.base.BaseActivity;
import com.appdev360.syncadapterdemo.ui.base.BaseFragment;
import com.appdev360.syncadapterdemo.ui.interfaces.UserClickListener;
import com.appdev360.syncadapterdemo.ui.mvpviews.UserView;
import com.appdev360.syncadapterdemo.ui.presenter.UserPresenter;
import com.appdev360.syncadapterdemo.ui.syncscreen.fragment.adapter.UserAdapter;

import java.util.List;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observer;
import rx.Subscription;


/**
 * Created by abubaker on 1/23/18.
 */

public class UserFragment extends BaseFragment implements UserView,UserClickListener{

    private static String TAG = UserFragment.class.getName();

    @BindView(R.id.user_recycler)
    RecyclerView mRecyclerView;
    @Inject
    UserPresenter userPresenter;
    @Inject
    UserAdapter userAda;
    @Inject
    RxBus rxBus;
    private LinearLayoutManager lytManager;

    Subscription mSubscription;
    Unbinder unbinder;
    private User user;

    @Override
    public void initViews(View parentView) {
        unbinder = ButterKnife.bind(this, parent);
        ((BaseActivity) getActivity()).activityComponent().inject(this);

        mSubscription = rxBus.toObservable().subscribe(observer);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user;
    }




    Observer observer = new Observer() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG,"onError "+e.getMessage());
        }

        @Override
        public void onNext(Object o) {

            if (o instanceof BaseModel){

                showUsers(((BaseModel) o).getData().getUsers());

            }

        }
    };


    @Override
    public void updateFragmentReference() {

        userPresenter.attachView(this);
        SyncManager.beginPeriodicSync(getActivity());
        mRecyclerView.setHasFixedSize(true);
        lytManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(lytManager);
        userAda.serOnClickListener(this);
        mRecyclerView.setAdapter(userAda);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

    }

    @Override
    public void updateActionState(boolean action, View v) {

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSubscription != null) mSubscription.unsubscribe();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void showUsers(List<User> users) {

        Toast.makeText(getActivity(), "showUsers",Toast.LENGTH_SHORT).show();

        if (userAda.getDataSet() != null) {
            userAda.getDataSet().clear();
        }

        if (users == null) {
            return;
        }else {
            userAda.setUserItems(users);
            userAda.notifyDataSetChanged();
        }


    }

    @Override
    public void showError(Error error) {

    }

    @Override
    public void noUserFound(String string) {
        Toast.makeText(getActivity(), string,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(int position) {

    }
}
