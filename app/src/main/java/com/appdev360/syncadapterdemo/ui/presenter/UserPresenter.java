package com.appdev360.syncadapterdemo.ui.presenter;

import android.content.Context;

import com.appdev360.syncadapterdemo.R;
import com.appdev360.syncadapterdemo.data.DataManager;
import com.appdev360.syncadapterdemo.data.enums.ErrorType;
import com.appdev360.syncadapterdemo.data.model.BaseModel;
import com.appdev360.syncadapterdemo.data.model.Data;
import com.appdev360.syncadapterdemo.data.model.Error;
import com.appdev360.syncadapterdemo.data.model.User;
import com.appdev360.syncadapterdemo.data.remote.NetworkService;
import com.appdev360.syncadapterdemo.injection.ActivityContext;
import com.appdev360.syncadapterdemo.injection.ApplicationContext;
import com.appdev360.syncadapterdemo.ui.base.BasePresenter;
import com.appdev360.syncadapterdemo.ui.mvpviews.UserView;
import com.appdev360.syncadapterdemo.util.GeneralUtils;
import com.appdev360.syncadapterdemo.util.RxUtil;

import java.util.List;

import javax.inject.Inject;


import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abubaker on 1/23/18.
 */

public class UserPresenter extends BasePresenter<UserView>{
    @Inject
    public  DataManager mDataManager;
    private  Context context;
    private Subscription mSubscription;
    private Error defaultError;
    private NetworkService networkService;


    @Inject
    public UserPresenter(@ApplicationContext Context context , DataManager dataManager) {
        mDataManager = dataManager;
        defaultError = new Error();
        defaultError.setHttpCode(DEFAULT_HTTP_CODE);
        defaultError.setMessage(DEFAULT_ERROR_MSG);
        this.context = context;
    }

    @Override
    public void attachView(UserView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void getUserList(){
        checkViewAttached();
        if (!GeneralUtils.isNetworkConnected(context)) {
            defaultError.setType(ErrorType.NETWORK);
            defaultError.setMessage(context.getString(R.string.network_error));
            getMvpView().showError(defaultError);
            return;
        }

        RxUtil.unsubscribe(mSubscription);
        mSubscription = networkService.getUserList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BaseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().showError(defaultError);
                    }

                    @Override
                    public void onNext(BaseModel model) {
                        parseResponse(model);
                    }
                });

    }

    private void parseResponse(BaseModel resp) {
        if (resp == null) {
            getMvpView().showError(defaultError);
            return;
        }
        if (resp.getMessage() != null && !resp.getMessage().equalsIgnoreCase("")) {
            defaultError.setMessage(resp.getMessage());
        }
 /*       if (resp.getHttpCode() != SUCCESS_HTTP_CODE) {
            getMvpView().showError(defaultError);
            return;
        }*/
        Data data = resp.getData();
        if (data == null) {
            getMvpView().noUserFound("No contacts found against your profile");
            return;
        }
        List<User> users = data.getUsers();
        if (users == null || users.size() == 0) {
            getMvpView().noUserFound("No contacts found against your profile");
            return;
        }
        getMvpView().showUsers(users);
    }

   /* public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }*/
}
