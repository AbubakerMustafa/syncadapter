package com.appdev360.syncadapterdemo.ui.syncscreen.fragment.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appdev360.syncadapterdemo.R;
import com.appdev360.syncadapterdemo.data.model.User;
import com.appdev360.syncadapterdemo.injection.ApplicationContext;
import com.appdev360.syncadapterdemo.ui.interfaces.UserClickListener;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jaffarraza on 05/06/2017.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ContactsViewHolder> {

    private final Context context;
    private List<User> userList;
    private UserClickListener listener;

    @Inject
    public UserAdapter(@ApplicationContext Context context) {
        this.context = context;
        userList = new ArrayList<>();
    }

    public void setUserItems(List<User> users) {
        userList = users;
    }

    public void addUser(List<User> user) {
        userList.addAll(user);

    }

    public void clear() {
        userList.clear();
        notifyDataSetChanged();
    }

    public List<User> getDataSet(){
        return userList;
    }


    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_layout, parent, false);
        return new ContactsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ContactsViewHolder holder, final int position) {

        final User user = userList.get(position);

        holder.name.setText(user.getName());

       /* if (user.getProfilePic() != null && !user.getProfilePic().equals("")) {
            Glide.with(context).load(user.getProfilePic()).into(holder.profileImage);
        }*/

        holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener == null) {
                    return;
                }
                listener.onClick(position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void serOnClickListener(UserClickListener listener) {
        this.listener = listener;
    }


    class ContactsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView profileImage;
        @BindView(R.id.delete_icon)
        ImageView deleteIcon;
        @BindView(R.id.name)
        TextView name;

        public ContactsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
