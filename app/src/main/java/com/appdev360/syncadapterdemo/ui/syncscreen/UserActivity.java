package com.appdev360.syncadapterdemo.ui.syncscreen;

import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.appdev.fragmentnavigation.FragNavController;
import com.appdev360.syncadapterdemo.R;
import com.appdev360.syncadapterdemo.ui.base.BaseActivity;
import com.appdev360.syncadapterdemo.ui.base.BaseFragment;
import com.appdev360.syncadapterdemo.ui.syncscreen.fragment.UserFragment;

import butterknife.ButterKnife;

public class UserActivity extends BaseActivity {

    @Override
    public void initViews(Bundle savedInstanceState) {

        activityComponent().inject(this);
        ButterKnife.bind(this);
        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.container)
                .transactionListener(this)
                .rootFragment(new UserFragment())
                .build();

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void pushFragment(Fragment fragment, boolean detach) {
        if (fragment != null && mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }


    @Override
    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            mNavController.replaceFragment(fragment);
        }
    }

    @Override
    public void navigationTitle(Fragment fragment) {
        if (fragment == null) {
            return;
        }
    /*    if (fragment instanceof Login) {
            setTitle("Login");
            return;
        }*/


    }

    @Override
    public void popCurrentFragment() {

    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {

    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
//        Timber.d("Fragment Popped", fragment.toString());
        navigationTitle(fragment);
        ((BaseFragment) fragment).updateFragmentReference();
    }
}
