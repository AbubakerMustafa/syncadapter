package com.appdev360.syncadapterdemo.ui.interfaces;

/**
 * Created by abubaker on 1/23/18.
 */

public interface UserClickListener {
    void onClick(int position);

}
