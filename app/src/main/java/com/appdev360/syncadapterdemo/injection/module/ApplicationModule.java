package com.appdev360.syncadapterdemo.injection.module;

import android.app.Application;
import android.content.Context;


import com.appdev360.syncadapterdemo.data.remote.NetworkService;
import com.appdev360.syncadapterdemo.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {

    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    NetworkService provideRibotsService() {
        return NetworkService.Creator.networkService();
    }



}
