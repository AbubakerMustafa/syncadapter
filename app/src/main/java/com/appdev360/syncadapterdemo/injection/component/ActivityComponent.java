package com.appdev360.syncadapterdemo.injection.component;

import com.appdev360.syncadapterdemo.injection.PerActivity;
import com.appdev360.syncadapterdemo.injection.module.ActivityModule;
import com.appdev360.syncadapterdemo.syncadapter.SyncAdapterService;
import com.appdev360.syncadapterdemo.ui.base.BaseFragment;
import com.appdev360.syncadapterdemo.ui.syncscreen.UserActivity;
import com.appdev360.syncadapterdemo.ui.syncscreen.fragment.UserFragment;

import dagger.Subcomponent;

/**
 * Created by abubaker on 1/23/18.
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {


    void inject(UserActivity userActivity);
    void inject(BaseFragment baseFragment);
    void inject(UserFragment userFragment);
   // void inject(SyncAdapterService userFragment);




}
