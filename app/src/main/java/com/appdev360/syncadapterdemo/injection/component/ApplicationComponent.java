package com.appdev360.syncadapterdemo.injection.component;

import android.app.Application;
import android.content.Context;


import com.appdev360.syncadapterdemo.data.DataManager;
import com.appdev360.syncadapterdemo.data.local.DatabaseRealm;
import com.appdev360.syncadapterdemo.data.local.PreferencesHelper;
import com.appdev360.syncadapterdemo.data.remote.NetworkService;
import com.appdev360.syncadapterdemo.injection.ApplicationContext;
import com.appdev360.syncadapterdemo.injection.module.ApplicationModule;
import com.appdev360.syncadapterdemo.rxbus.RxBus;
import com.appdev360.syncadapterdemo.syncadapter.SyncAdapter;


import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();

    NetworkService newtworkService();

    PreferencesHelper preferencesHelper();

    DataManager dataManager();

    void inject(SyncAdapter syncAdapter);

    RxBus eventBus();

    DatabaseRealm databaseRealm();

}
