package com.appdev360.syncadapterdemo.data.local;

/**
 * Created by abubaker on 1/23/18.
 */

public interface UserDataHelper {

    public void clearAllPref();

/*    public void storeUserObject(User userObject);

    public User getUserObject();*/

    public void storeToken(String token);

    public String getToken();

}
