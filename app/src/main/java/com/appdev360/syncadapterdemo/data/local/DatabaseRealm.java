package com.appdev360.syncadapterdemo.data.local;

import android.content.Context;


import com.appdev360.syncadapterdemo.injection.ApplicationContext;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by ahsan on 4/24/17.
 */
@Singleton
public class DatabaseRealm {

    private Context mContext;

    RealmConfiguration realmConfiguration;

    @Inject
    public DatabaseRealm(@ApplicationContext Context context) {
        this.mContext = context;
        setup();
    }

    private void setup() {
        if (realmConfiguration == null) {
            Realm.init(mContext);
            // create your Realm configuration
            realmConfiguration = new RealmConfiguration.
                    Builder().
                    deleteRealmIfMigrationNeeded().
                    build();
            Realm.setDefaultConfiguration(realmConfiguration);

        } else {
            throw new IllegalStateException("database already configured");
        }
    }

    public Realm getRealmInstance() {
        return Realm.getDefaultInstance();
    }

    public <T extends RealmObject> T add(T model) {
        Realm realm = getRealmInstance();
        realm.beginTransaction();
        realm.copyToRealm(model);
        realm.commitTransaction();
        return model;
    }

    public <T extends RealmObject> List<T> findAll(Class<T> clazz) {
        return getRealmInstance().where(clazz).findAll();
    }

    public void close() {
        getRealmInstance().close();
    }

/*    public Observable<User> setUsers(final List<User> users) {
        return Observable.create(new Observable.OnSubscribe<User>() {
            @Override
            public void call(Subscriber<? super User> subscriber) {
                if (subscriber.isUnsubscribed()) return;
//                Realm realm = getRealmInstance();
//                realm.beginTransaction();
//                for (User user : users) {
//                    realm.copyToRealm(user);
//                }
//                realm.commitTransaction();
//                subscriber.onCompleted();

            }
        });
    }*/

//    public Observable<List<User>> getUsers() {
//        return Observable.just(getRealmInstance().copyFromRealm(getRealmInstance().where(User.class).findAll()));
//
//    }
}
