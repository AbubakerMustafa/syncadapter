package com.appdev360.syncadapterdemo.data.model;


import com.appdev360.syncadapterdemo.data.enums.ErrorType;

import java.io.Serializable;

/**
 * Created by jaffarraza on 15/05/2017.
 */

public class Error implements Serializable {

    private int status;
    private int httpCode;
    private String message;
    private ErrorType type = ErrorType.NORMAL;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public ErrorType getType() {
        return type;
    }

    public void setType(ErrorType type) {
        this.type = type;
    }
}