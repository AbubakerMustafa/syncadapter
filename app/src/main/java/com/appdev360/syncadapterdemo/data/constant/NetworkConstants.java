package com.appdev360.syncadapterdemo.data.constant;

/**
 * Created by abubaker on 1/23/18.
 */

public class NetworkConstants {

    public static final int TIME_OUT = 30;
    public static final String BASE_URL = "http://www.mocky.io/v2/";
    public static final String USER_LIST = "5a675bc42d00005b16becf07";
    public static final String AUTHORIZATION_PARAM = "Authorization";

}
