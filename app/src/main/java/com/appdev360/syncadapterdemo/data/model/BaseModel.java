package com.appdev360.syncadapterdemo.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jaffarraza on 16/05/2017.
 */

public class BaseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("httpCode")
    private int httpCode;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}