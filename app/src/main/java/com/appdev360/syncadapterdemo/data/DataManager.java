package com.appdev360.syncadapterdemo.data;

import android.content.Context;

import com.appdev360.syncadapterdemo.data.local.DatabaseRealm;
import com.appdev360.syncadapterdemo.data.local.PreferencesHelper;
import com.appdev360.syncadapterdemo.data.remote.NetworkService;
import com.appdev360.syncadapterdemo.injection.ApplicationContext;

import javax.inject.Inject;

/**
 * Created by abubaker on 1/23/18.
 */

public class DataManager {

    private final NetworkService mNetWorkService;
    private final PreferencesHelper mPreferencesHelper;
    private final DatabaseRealm mDatabaseRealm;

    @Inject
    public DataManager(NetworkService networkService, PreferencesHelper preferencesHelper, DatabaseRealm
            databaseRealm, @ApplicationContext Context context) {
        mNetWorkService = networkService;
        mPreferencesHelper = preferencesHelper;
        this.mDatabaseRealm = databaseRealm;
        // this.mDatabaseRealm.setmContext(context);

    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public NetworkService getNetworkService() {
        return mNetWorkService;
    }

    public DatabaseRealm getRealm() {
        return mDatabaseRealm;
    }

}
