package com.appdev360.syncadapterdemo.data.remote;

import com.appdev360.syncadapterdemo.data.constant.GeneralConstants;
import com.appdev360.syncadapterdemo.data.constant.NetworkConstants;
import com.appdev360.syncadapterdemo.data.model.BaseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;
import rx.Observable;

import static com.appdev360.syncadapterdemo.data.constant.NetworkConstants.USER_LIST;

/**
 * Created by abubaker on 1/23/18.
 */

public interface NetworkService {


    @POST(USER_LIST)
    Observable<BaseModel> getUserList();


    /******** Helper class that sets up a new services *******/
    class Creator {

        public static NetworkService networkService() {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(NetworkConstants.TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(NetworkConstants.TIME_OUT, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat(GeneralConstants.DEFAULT_TIME_FORMAT)
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(NetworkConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build();
            return retrofit.create(NetworkService.class);
        }
    }
}
