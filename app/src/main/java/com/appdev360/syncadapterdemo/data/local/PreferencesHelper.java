package com.appdev360.syncadapterdemo.data.local;

import android.content.Context;

import com.appdev360.syncadapterdemo.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class PreferencesHelper implements UserDataHelper {

    private Context context;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        this.context=context;
    }


    @Override
    public void clearAllPref() {
        PreferencesDataHelper.clearPref(context);
    }

  /*  @Override
    public void storeUserObject(User userObject) {
        Gson gson = new Gson();
        String userJson = gson.toJson(userObject);
        PreferencesDataHelper.store(context, PreferencesDataHelper.PersistenceKey.LOGGEDIN_USER, userJson);

    }

    @Override
    public User getUserObject() {
        try {
            String userJson =  PreferencesDataHelper.retrieve(context, PreferencesDataHelper.PersistenceKey.LOGGEDIN_USER);
            if (userJson == null || userJson.equals("")) {
                return null;
            }
            Gson gson = new Gson();
            User user = gson.fromJson(userJson, User.class);
            return user;
        } catch (Exception e) {
            return null;
        }
    }*/

    @Override
    public void storeToken(String token) {
        PreferencesDataHelper.store(context, PreferencesDataHelper.PersistenceKey.TOKEN, token);
    }

    @Override
    public String getToken() {
        return PreferencesDataHelper.retrieve(context, PreferencesDataHelper.PersistenceKey.TOKEN);
    }

/*    @Override
    public boolean getLoggedInType() {
        return PreferencesDataHelper.retrieveBoolean(context, PreferencesDataHelper.PersistenceKey.IS_SERVER);
    }

    @Override
    public void setLoggedInType(boolean isServer) {
        PreferencesDataHelper.store(context, PreferencesDataHelper.PersistenceKey.IS_SERVER, isServer);
    }*/

    public void setTerms(boolean terms) {
        PreferencesDataHelper.store(context, PreferencesDataHelper.PersistenceKey.TOS, terms);
    }

    public boolean getTos() {
        return PreferencesDataHelper.retrieveBoolean(context, PreferencesDataHelper.PersistenceKey.TOS);
    }
}
