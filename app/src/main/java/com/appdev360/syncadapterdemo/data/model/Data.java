package com.appdev360.syncadapterdemo.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abubaker on 1/23/18.
 */

public class Data {

    @SerializedName("users")
    @Expose
    private List<User> users = null;


    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
